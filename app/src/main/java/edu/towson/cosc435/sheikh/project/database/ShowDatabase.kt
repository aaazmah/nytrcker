package edu.towson.cosc435.sheikh.project.database

import androidx.room.*
import edu.towson.cosc435.sheikh.project.data_model.Shows
import java.util.*


// Database queries
@Dao
interface InterfaceShowDao{

    @Query("select id, name, posterPath from Shows")
    fun getShows(): List<Shows>

    @Query("Select * from Shows where name = :name")
    fun getShow(name: String) : Shows

    @Update
    fun updateShow(shows: Shows)

    @Delete
    fun deleteShow(shows: Shows)

    @Insert
    fun addShow(shows: Shows)


}


class UUIDTypeConverter{
    @TypeConverter
    fun toString(uuid: UUID): String{
        return uuid.toString()
    }

    @TypeConverter
    fun fromString(uuid: String) : UUID {
        return  UUID.fromString(uuid)
    }
}

@Database(entities = [Shows::class], version = 1, exportSchema = false)
@TypeConverters(UUIDTypeConverter::class)
abstract class ShowDatabase : RoomDatabase(){
    abstract fun showDao(): InterfaceShowDao
}