package edu.towson.cosc435.sheikh.project.database

import android.app.Application
import androidx.room.Room
import edu.towson.cosc435.sheikh.project.ShowList
import edu.towson.cosc435.sheikh.project.data_model.Shows
import edu.towson.cosc435.sheikh.project.interfaces.ShowInterface

class ShowDatabaseRepository (app: Application) : ShowInterface{

    private val showList: MutableList<Shows> = mutableListOf()
    private val showDb: ShowDatabase

    init{
        showDb = Room.databaseBuilder(
            app,
            ShowDatabase::class.java,
            "shows.db"
        ).allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
        showList.addAll(showDb.showDao().getShows())
    }


    override fun getCount(): Int {
        return showList.size
    }

    override fun getShow(idx: Int): Shows {
        return showList[idx]
    }

    override fun getShows(): List<Shows> {
        return showList
    }

    override fun deleteShow(idx: Int) {
        val show = showList[idx]
        showDb.showDao().deleteShow(show)
        showList.clear()
        showList.addAll(showDb.showDao().getShows())
    }

    override fun updateShow(idx: Int, show: Shows){
        showDb.showDao().updateShow(show)
        showList.clear()
        showList.addAll(showDb.showDao().getShows())
    }

    override fun addShow(show: Shows){
        showDb.showDao().addShow(show)
        showList.clear()
        showList.addAll(showDb.showDao().getShows())
    }
}