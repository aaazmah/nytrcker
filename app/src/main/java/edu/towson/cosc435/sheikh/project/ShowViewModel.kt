package edu.towson.cosc435.sheikh.project

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import edu.towson.cosc435.sheikh.project.data_model.Shows
import edu.towson.cosc435.sheikh.project.repository.NetworkState
import io.reactivex.disposables.CompositeDisposable


class ShowViewModel(private val showRepository : ShowPagedRepository) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    val  showPagedList : LiveData<PagedList<Shows>> by lazy {
        showRepository.fetchLiveShowPagedList(compositeDisposable)
    }

    val  networkState : LiveData<NetworkState> by lazy {
        showRepository.getNetworkState()
    }

    fun listIsEmpty(): Boolean {
        return showPagedList.value?.isEmpty() ?: true
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}