package edu.towson.cosc435.sheikh.project;

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import edu.towson.cosc435.sheikh.project.api.POSTER_BASE_URL
import edu.towson.cosc435.sheikh.project.data_model.Shows
import edu.towson.cosc435.sheikh.project.repository.NetworkState
import edu.towson.cosc435.sheikh.test.R
import kotlinx.android.synthetic.main.network_state_item.view.*
import kotlinx.android.synthetic.main.shows.view.*

class showsAdapter(val context: Context) : PagedListAdapter<Shows, RecyclerView.ViewHolder>(showCallback()) {

        val SHOW_VIEW = 1
        val NETWORK_VIEW_TYPE = 2
        private var networkState: NetworkState? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view: View
                if (viewType == SHOW_VIEW) {
                        view = layoutInflater.inflate(R.layout.shows, parent, false)
                        return ShowItemViewHolder(view)
                } else {
                        view = layoutInflater.inflate(R.layout.network_state_item, parent, false)
                        return NetworkStateItemViewHolder(view)
                }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
                if (getItemViewType(position) == SHOW_VIEW) {
                        (holder as ShowItemViewHolder).bind(getItem(position),context)
                }
                 else {
                        (holder as NetworkStateItemViewHolder).bind(networkState)
                 }
        }


        private fun hasExtraRow(): Boolean {
                return networkState != null && networkState != NetworkState.LOADED
        }


        override fun getItemCount(): Int {
                return super.getItemCount() + if (hasExtraRow()) 1 else 0
        }

        override fun getItemViewType(position: Int): Int {
                return if (hasExtraRow() && position == itemCount - 1) {
                        NETWORK_VIEW_TYPE
                } else {
                        SHOW_VIEW
                }
        }




class showCallback : DiffUtil.ItemCallback<Shows>() {

        override fun areItemsTheSame(oldItem: Shows, newItem: Shows): Boolean {
                return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Shows, newItem: Shows): Boolean {
                return oldItem == newItem
        }

}


class ShowItemViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        fun bind(show: Shows?, context: Context) {
        itemView.show_name.text = show?.name

        val moviePosterURL = POSTER_BASE_URL + show?.posterPath
                 Glide.with(itemView.context)
                .load(moviePosterURL)
                .into(itemView.show_poster);
        }
}

class NetworkStateItemViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        fun bind(networkState: NetworkState?) {
                if (networkState != null && networkState == NetworkState.LOADING) {
                        itemView.progress_bar_item.visibility = View.VISIBLE;
                }
                else  {
                        itemView.progress_bar_item.visibility = View.GONE;
                }
                if (networkState != null && networkState == NetworkState.ERROR) {
                        itemView.error_msg_item.visibility = View.VISIBLE;
                        itemView.error_msg_item.text = networkState.msg;
                }
                else if (networkState != null && networkState == NetworkState.ENDOFLIST) {
                        itemView.error_msg_item.visibility = View.VISIBLE;
                        itemView.error_msg_item.text = networkState.msg;
                }
                else {
                        itemView.error_msg_item.visibility = View.GONE;
                }
        }
}

        fun setNetworkState(newNetworkState: NetworkState) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
                if (hadExtraRow != hasExtraRow) {
                        if (hadExtraRow) {                             //hadExtraRow is true and hasExtraRow false
                        notifyItemRemoved(super.getItemCount())    //remove the progressbar at the end
                        } else {                                       //hasExtraRow is true and hadExtraRow false
                        notifyItemInserted(super.getItemCount())   //add the progressbar at the end
                        }
                        } else if (hasExtraRow && previousState != newNetworkState) { //hasExtraRow is true and hadExtraRow true and (NetworkState.ERROR or NetworkState.ENDOFLIST)
                        notifyItemChanged(itemCount - 1)       //add the network message at the end
                }
        }
}