package edu.towson.cosc435.sheikh.project

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import edu.towson.cosc435.sheikh.project.api.POST_PER_PAGE
import edu.towson.cosc435.sheikh.project.api.ShowApi_Interface
import edu.towson.cosc435.sheikh.project.data_model.Shows
import edu.towson.cosc435.sheikh.project.repository.NetworkState
import edu.towson.cosc435.sheikh.project.repository.ShowDataSource
import edu.towson.cosc435.sheikh.project.repository.ShowDataSourceFactory
import io.reactivex.disposables.CompositeDisposable

class ShowPagedRepository (private val apiService : ShowApi_Interface) {

    lateinit var showPageList: LiveData<PagedList<Shows>>
    lateinit var showDataSourceFactory: ShowDataSourceFactory

    fun fetchLiveShowPagedList (compositeDisposable: CompositeDisposable) : LiveData<PagedList<Shows>> {
        showDataSourceFactory = ShowDataSourceFactory(apiService, compositeDisposable)

        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(POST_PER_PAGE)
            .build()

        showPageList = LivePagedListBuilder(showDataSourceFactory, config).build()

        return showPageList
    }

    fun getNetworkState(): LiveData<NetworkState> {
        return Transformations.switchMap<ShowDataSource, NetworkState>(
            showDataSourceFactory.showsLiveDataSource, ShowDataSource::networkState)
    }
}