package edu.towson.cosc435.sheikh.test

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.Toast
import com.google.android.material.floatingactionbutton.FloatingActionButton
import edu.towson.cosc435.sheikh.project.ShowList

class Watching : AppCompatActivity(),  View.OnClickListener{

    private lateinit var mToolbar: androidx.appcompat.widget.Toolbar
    private lateinit var addButton: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_watching)
        widgets()

        addButton.setOnClickListener(this)
        mToolbar = findViewById(R.id.main_toolbar)
        setSupportActionBar(mToolbar)
    }

    fun widgets(){
        addButton = findViewById(R.id.addButton)
    }

    private fun launchShowListActivity(){
        val intent6 = Intent(this, ShowList::class.java)
        startActivity(intent6)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)

    }

    override fun onClick(view: View?) {
        try {
            when(view?.id){
                R.id.addButton -> launchShowListActivity()
            }
        }catch (e: Exception) {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
        }
    }


}