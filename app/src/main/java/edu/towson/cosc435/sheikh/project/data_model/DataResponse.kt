package edu.towson.cosc435.sheikh.project.data_model


import com.google.gson.annotations.SerializedName

data class DataResponse(
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val showResults: List<Shows>,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int
)