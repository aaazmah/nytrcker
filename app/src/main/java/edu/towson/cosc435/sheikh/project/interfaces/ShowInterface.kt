package edu.towson.cosc435.sheikh.project.interfaces

import edu.towson.cosc435.sheikh.project.data_model.Shows

interface ShowInterface {
    fun getCount(): Int
    fun getShow(idx: Int): Shows
    fun getShows(): List<Shows>
    fun deleteShow(idx: Int)
    fun updateShow(idx: Int, show: Shows)
    fun addShow(show: Shows)

}