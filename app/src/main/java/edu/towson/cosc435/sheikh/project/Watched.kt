package edu.towson.cosc435.sheikh.test

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import edu.towson.cosc435.sheikh.project.ShowList
import edu.towson.cosc435.sheikh.project.ShowPagedRepository
import edu.towson.cosc435.sheikh.project.ShowViewModel
import edu.towson.cosc435.sheikh.project.api.API_Service
import edu.towson.cosc435.sheikh.project.api.ShowApi_Interface
import edu.towson.cosc435.sheikh.project.repository.NetworkState
import edu.towson.cosc435.sheikh.project.showsAdapter
import kotlinx.android.synthetic.main.activity_watched.*


class Watched : AppCompatActivity(), View.OnClickListener {


    private lateinit var mToolbar: androidx.appcompat.widget.Toolbar
    private lateinit var addButton: FloatingActionButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_watched)

        widgets()

        addButton.setOnClickListener(this)

        mToolbar = findViewById(R.id.main_toolbar)
        setSupportActionBar(mToolbar)

    }

    fun widgets(){
        addButton = findViewById(R.id.addButton)
    }

    private fun launchShowListActivity(){
        val intent5 = Intent(this, ShowList::class.java)
        startActivity(intent5)
    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)

    }

    override fun onClick(view: View?) {
        try {
            when(view?.id){
                R.id.addButton -> launchShowListActivity()
            }
        }catch (e: Exception) {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
        }
    }
}
