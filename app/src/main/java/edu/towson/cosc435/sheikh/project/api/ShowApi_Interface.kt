package edu.towson.cosc435.sheikh.project.api

import edu.towson.cosc435.sheikh.project.data_model.DataResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

// Get request at the endpoint

interface ShowApi_Interface {
    @GET("tv/popular")
    fun getShows(@Query("page") page: Int): Single<DataResponse>
}