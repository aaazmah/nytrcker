package edu.towson.cosc435.sheikh.project.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import edu.towson.cosc435.sheikh.project.api.ShowApi_Interface
import edu.towson.cosc435.sheikh.project.data_model.Shows
import io.reactivex.disposables.CompositeDisposable

class ShowDataSourceFactory (private val apiService : ShowApi_Interface, private val compositeDisposable: CompositeDisposable)
    : DataSource.Factory<Int, Shows>() {

    val showsLiveDataSource =  MutableLiveData<ShowDataSource>()

    override fun create(): DataSource<Int, Shows> {
        val showDataSource = ShowDataSource(apiService,compositeDisposable)

        showsLiveDataSource.postValue(showDataSource)
        return showDataSource
    }
}