package edu.towson.cosc435.sheikh.project

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import edu.towson.cosc435.sheikh.project.api.API_Service
import edu.towson.cosc435.sheikh.project.api.ShowApi_Interface
import edu.towson.cosc435.sheikh.project.repository.NetworkState
import edu.towson.cosc435.sheikh.test.R
import kotlinx.android.synthetic.main.activity_watched.*
import androidx.lifecycle.Observer


class ShowList : AppCompatActivity() {

    private lateinit var viewModel: ShowViewModel
    lateinit var showRepository: ShowPagedRepository
    private lateinit var mToolbar: androidx.appcompat.widget.Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_list)
        mToolbar = findViewById(R.id.main_toolbar)
        setSupportActionBar(mToolbar)

        val apiService : ShowApi_Interface = API_Service.getClient()

        showRepository = ShowPagedRepository(apiService)

        viewModel = getViewModel()

        val adapter = showsAdapter(this)

        val gridLayoutManager = GridLayoutManager(this, 1)

        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val viewType = adapter.getItemViewType(position)
                if (viewType == adapter.SHOW_VIEW) return  1    // Movie_VIEW_TYPE will occupy 1 out of 3 span
                else return 3                                              // NETWORK_VIEW_TYPE will occupy all 3 span
            }
        };


        show_list.layoutManager = gridLayoutManager
        show_list.setHasFixedSize(true)
        show_list.adapter = adapter

        viewModel.showPagedList.observe(this, Observer {
            adapter.submitList(it)
        })

        viewModel.networkState.observe(this, Observer {
            progress_bar_popular.visibility = if (viewModel.listIsEmpty() && it == NetworkState.LOADING) View.VISIBLE else View.GONE
            txt_error_popular.visibility = if (viewModel.listIsEmpty() && it == NetworkState.ERROR) View.VISIBLE else View.GONE

            if (!viewModel.listIsEmpty()) {
                adapter.setNetworkState(it)
            }
        })

    }


    private fun getViewModel(): ShowViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return ShowViewModel(showRepository) as T
            }
        })[ShowViewModel::class.java]
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)

    }

}