package edu.towson.cosc435.sheikh.project.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import edu.towson.cosc435.sheikh.project.api.FIRST_PAGE
import edu.towson.cosc435.sheikh.project.api.ShowApi_Interface
import edu.towson.cosc435.sheikh.project.data_model.Shows
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ShowDataSource (private val apiService : ShowApi_Interface, private val compositeDisposable: CompositeDisposable)
    : PageKeyedDataSource<Int, Shows>(){

    private var page = FIRST_PAGE

    val networkState: MutableLiveData<NetworkState> = MutableLiveData()


    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Shows>) {

        networkState.postValue(NetworkState.LOADING)

        compositeDisposable.add(
            apiService.getShows(page)
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        callback.onResult(it.showResults, null, page+1)
                        networkState.postValue(NetworkState.LOADED)
                    },
                    {
                        networkState.postValue(NetworkState.ERROR)
                        it.message?.let { it1 -> Log.e("ShowDataSource", it1) }
                    }
                )
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Shows>) {
        networkState.postValue(NetworkState.LOADING)

        compositeDisposable.add(
            apiService.getShows(params.key)
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        if(it.totalPages >= params.key) {
                            callback.onResult(it.showResults, params.key+1)
                            networkState.postValue(NetworkState.LOADED)
                        }
                        else{
                            networkState.postValue(NetworkState.ENDOFLIST)
                        }
                    },
                    {
                        networkState.postValue(NetworkState.ERROR)
                        it.message?.let { it1 -> Log.e("ShowDataSource", it1) }
                    }
                )
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Shows>) {

    }
}