package edu.towson.cosc435.sheikh.test

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.Button
import android.widget.Toast



class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var watchedButton: Button
    private lateinit var watchingButton: Button
    private lateinit var willWatchButton: Button


    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // widget function is called after main activity, above, starts
        widgets()

        // attach a on click listener so the button can be pressed
        watchedButton.setOnClickListener(this)
        watchingButton.setOnClickListener(this)
        willWatchButton.setOnClickListener(this)


    }

    private fun widgets(){
        watchedButton = findViewById(R.id.watched)
        watchingButton = findViewById(R.id.watching)
        willWatchButton = findViewById(R.id.willWatch)
    }

    private fun launchWatchedActivity(){
        val intent = Intent(this, Watched::class.java)
        startActivity(intent)
    }

    private fun launchWatchingActivity(){
        val intent_two = Intent(this, Watching::class.java)
        startActivity(intent_two)
    }

    private fun launchWillWatchActivity(){
        val intent_three = Intent(this, WillWatch::class.java)
        startActivity(intent_three)
    }

    override fun onClick(view: View?) {
        try {
            when(view?.id){
                R.id.watched -> launchWatchedActivity()
                R.id.watching -> launchWatchingActivity()
                R.id.willWatch -> launchWillWatchActivity()
            }
        }catch (e: Exception) {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
        }
    }


}

